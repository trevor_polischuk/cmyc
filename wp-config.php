<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cmyc');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'P)~0IWJBpq1PW#R6QJT]-;{*yt7/9dVR;`eu?BpPj;Q%eY(js=>vxg-bK3I6PBKH');
define('SECURE_AUTH_KEY',  ':t,F:PtZluwyFy&*KYfbFW:T3p#iE)Wz2*%{`rp,$Kj ]!Zds&M#sb@Aa~|~++MR');
define('LOGGED_IN_KEY',    'U!gsyW%9JmsRP^0.[;}u73^5 q[bs2L54,j98|T9#x5z|)2LKLEZ}46%Rq6F>GF0');
define('NONCE_KEY',        'ke?-frP1xUmt=g=zVMW!J>e5tNDO&}L2D-bwcRL wmow*`P9&IUO]CeX~7/[=T<5');
define('AUTH_SALT',        'ooZxR1B65td_f?T[N?ZA/U[jkMt.Z7D@+v>8TIilAV-<>Ub(#~Yg`@IC;n|`@/&6');
define('SECURE_AUTH_SALT', 'q<X+cyw7RWZzawKr0m+OPE*5I+ma;r@z-rCMpT9=YW.EJ}|6Vf?YEpiwx+*09wev');
define('LOGGED_IN_SALT',   'Kn4$A;f#YsPh[@|>od 4U#N>9wbLi%@_Lf+1Z);SQj|&Rq&CVQsec/}3~f$T/ Ck');
define('NONCE_SALT',       '}3q^Q#3E_FjeUp_>$yK4v3B)=|d.@&3b+Ra)pGK7|IllfS6CI=huXxo=W<62rFTJ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
